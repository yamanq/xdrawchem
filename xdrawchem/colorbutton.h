// XDrawChem
// Copyright (C) 2006  Gerd Fleischer <gerdfleischer@gmx.de>
// Copyright (C) 2020  Yaman Qalieh <ybq987@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef COLOR_BUTTON_H
#define COLOR_BUTTON_H

#include <QPushButton>

class QColor;
class QPixmap;
class QStyleOptionButton;

class ColorButton : public QPushButton {
    Q_OBJECT

  public:
    ColorButton(QColor color = Qt::black, QWidget *parent = 0);
    // default destructor

    void setColor(QColor color);

  private:
    void paintEvent(QPaintEvent *);
    void initStyleOption(QStyleOptionButton *opt) const;

    QColor btnColor;
};

#endif
