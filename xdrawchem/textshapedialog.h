// XDrawChem
// Copyright (C) 2004-2005  Bryan Herger <bherger@users.sourceforge.net>
// Copyright (C) 2020  Yaman Qalieh <ybq987@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef TSDIALOG_H
#define TSDIALOG_H

#include <QCheckBox>
#include <QComboBox>
#include <QDialog>
#include <QLineEdit>

class TextShapeDialog : public QDialog {
    Q_OBJECT

  public:
    TextShapeDialog(QWidget *parent);
    void setBorderColor(QColor);
    void setFillColor(QColor);
    QColor getBorderColor() { return bc; }
    QColor getFillColor() { return fc; }
    void setBorderCheck(bool b1) { borderCheck->setChecked(b1); }
    void setFillCheck(bool b1) { fillCheck->setChecked(b1); }
    bool getBorderCheck() { return borderCheck->isChecked(); }
    bool getFillCheck() { return fillCheck->isChecked(); }
    void set_stype(int s1) { stype->setCurrentIndex(s1); }
    int get_stype() { return stype->currentIndex(); }

    void setWidth(int n1) {
        QString nx;
        nx.setNum(n1);
        swidth->setText(nx);
    }

    void setHeight(int n1) {
        QString nx;
        nx.setNum(n1);
        sheight->setText(nx);
    }

    int getWidth() { return swidth->text().toInt(); }
    int getHeight() { return sheight->text().toInt(); }

  public slots:
    void editFillColor();
    void editBorderColor();

  private:
    QComboBox *stype;
    QPushButton *bbutton, *fbutton;
    QCheckBox *borderCheck, *fillCheck;
    QLineEdit *swidth, *sheight;
    QColor fc, bc;
};

#endif
