// XDrawChem
// Copyright (C) 2004-2005  Bryan Herger <bherger@users.sourceforge.net>
// Copyright (C) 2020  Yaman Qalieh <ybq987@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// ngw.h -- class definition for 'new' XDrawChem NMR/MS/IR graph widget.

#ifndef NEWGRAPHWIDGET_H
#define NEWGRAPHWIDGET_H

#include <QPixmap>
#include <QWidget>

#include "graphdata.h"
#include "peak.h"

#define DATA_1HNMR 1
#define DATA_13CNMR 2
#define DATA_IR 3

class QPaintEvent;
class QPrinter;

class NewGraphWidget : public QWidget {
    Q_OBJECT

  public:
    NewGraphWidget(QWidget *parent = 0);
    void setDataType(int x) { datatype = x; }
    void AddPeak(double, QColor, QString l1 = QString(), QString t1 = QString());
    void AddPeak(double, int, QColor, QString l1 = QString(), QString t1 = QString());
    void AddPeak(Peak *, QColor);
    void AddPixmap(QPixmap p) { p1 = p; }
    void PrintSetup();

  public slots:
    void Print();

  protected:
    void paintEvent(QPaintEvent *);

  private:
    int datatype;
    int output;
    QList<GraphData *> peaks;
    QPixmap p1;
    QPrinter *printer;
};

#endif
