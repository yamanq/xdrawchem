// XDrawChem
// Copyright (C) 2004-2005  Bryan Herger <bherger@users.sourceforge.net>
// Copyright (C) 2020  Yaman Qalieh <ybq987@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef HELPWINDOW_H
#define HELPWINDOW_H

#include <QMainWindow>

class QComboBox;
class QTextBrowser;
class QUrl;

class HelpWindow : public QMainWindow {
    Q_OBJECT

  public:
    HelpWindow(const QString &home_, const QString &path, QWidget *parent = 0);
    ~HelpWindow();

  private slots:
    void setBackwardAvailable(bool);
    void setForwardAvailable(bool);

    void sourceChanged(const QUrl &);
    void about();
    void print();

    void pathSelected(const QString &);

  private:
    QTextBrowser *browser;
    QComboBox *pathCombo;
    QAction *backwardAction, *forwardAction, *homeAction;
    QString selectedURL;
};

#endif
