// XDrawChem
// Copyright (C) 2004-2005  Bryan Herger <bherger@users.sourceforge.net>
// Copyright (C) 2020  Yaman Qalieh <ybq987@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// gobject.h -- subclass of Drawable for graphic objects

#ifndef GOBJECT_H
#define GOBJECT_H

#include "dpoint.h"
#include "drawable.h"
#include "render2d.h"

class QRect;
class QString;
class QPolygon;

class GraphicObject : public Drawable {
  public:
    GraphicObject(Render2D *, QObject *parent = 0);
    GraphicObject *CloneTo(Drawable *target = nullptr) const;
    void Render(); // draw this object
    void Edit();
    int Type();          // return type of object
    bool Find(DPoint *); // does this GraphicObject contain this DPoint?
    DPoint *FindNearestPoint(DPoint *, double &);
    Drawable *FindNearestObject(DPoint *, double &);
    void setPoints(DPoint *, DPoint *);
    void setPointArray(QPolygon);
    bool isWithinRect(QRect, bool);
    QRect BoundingBox();
    QString ToXML(QString);
    QString ToCDXML(QString);
    void FromXML(QString);
    int Style() { return style; }
    void SetStyle(int s) { style = s; }
    int objectType() { return ot; }
    void setObjectType(int t) { ot = t; }

  private:
    // Renderer
    Render2D *r;
    // Point array (for cubic bezier and possibly others)
    QPolygon objectPoints;
    // graphic object type
    int ot;
};

#endif
