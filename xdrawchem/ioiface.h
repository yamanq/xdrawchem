// XDrawChem
// Copyright (C) 2002  Adam Tenderholt <atenderholt@users.sourceforge.net>
// Copyright (C) 2004-2005  Bryan Herger <bherger@users.sourceforge.net>
// Copyright (C) 2020  Yaman Qalieh <ybq987@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Interface between ChemData and OELib

#ifndef _IOIFACE_H
#define _IOIFACE_H

#include "chemdata.h"

#include <openbabel/atom.h>
#include <openbabel/bond.h>
#include <openbabel/math/vector3.h>
#include <openbabel/mol.h>

using namespace OpenBabel;

class IOIface {

  public:
    IOIface(ChemData *cd = 0, OBMol *mol = 0);
    ~IOIface();

    static const char symbol[110][4];

    void setChemData(ChemData *cd);
    void setOBMol(OBMol *mol);
    bool convertToChemData();
    bool convertToOBMol();

  private:
    ChemData *chemdata;
    OBMol *obmol;
};

#endif

// eof
