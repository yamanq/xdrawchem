// XDrawChem
// Copyright (C) 2004-2005  Bryan Herger <bherger@users.sourceforge.net>
// Copyright (C) 2020  Yaman Qalieh <ybq987@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// arrow.h -- subclass of Drawable for arrows

#ifndef ARROW_H
#define ARROW_H

#include <QPoint>
#include <QRect>

#include "dpoint.h"
#include "drawable.h"
#include "render2d.h"

class Arrow : public Drawable {
    Q_OBJECT

  public:
    Arrow(Render2D *, QObject *parent = 0);
    Arrow *CloneTo(Drawable *target = nullptr) const;
    void Render();       // draw this object
    void Edit();         // edit this object
    int Type();          // return type of object
    bool Find(DPoint *); // does this Arrow contain this DPoint?
    DPoint *FindNearestPoint(DPoint *, double &);
    Drawable *FindNearestObject(DPoint *, double &);
    void setPoints(DPoint *, DPoint *);
    QRect BoundingBox();
    bool isWithinRect(QRect, bool);
    QString ToXML(QString);
    QString ToCDXML(QString);
    void FromXML(QString);
    int Orientation();
    QPoint Midpoint();
    int Style() { return style; }
    void SetStyle(int s1) { style = s1; }

  private:
    // Renderer
    Render2D *m_renderer;
};

#endif
