// XDrawChem
// Copyright (C) 2004-2005  Bryan Herger <bherger@users.sourceforge.net>
// Copyright (C) 2020  Yaman Qalieh <ybq987@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <cstdlib>

#include <QCheckBox>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QSpacerItem>

#include "defs.h"
#include "netdialog.h"
//#include "http.h"

NetDialog::NetDialog(QWidget *parent) : QDialog(parent) {
    QString serverName;

    serverName = getenv("XDC_SERVER");

    if (serverName.length() < 2)
        serverName = XDC_SERVER;

    QLabel *cap1, *caption, *l1;

    setWindowTitle(tr("Find structure via Internet"));

    QGridLayout *mygrid = new QGridLayout(this);

    cap1 = new QLabel(tr("XDC database server:"));
    mygrid->addWidget(cap1, 0, 0, 1, 4);

    serverkey = new QLineEdit();
    serverkey->setText(serverName);
    mygrid->addWidget(serverkey, 0, 4, 1, 4);

    caption = new QLabel(tr("Search type:"));
    mygrid->addWidget(caption, 1, 0, 1, 4);

    keylist = new QComboBox();
    keylist->addItem(tr("Chemical name"));
    keylist->addItem(tr("CAS Number"));
    keylist->addItem(tr("Formula"));
    mygrid->addWidget(keylist, 1, 4, 1, 4);

    l1 = new QLabel(tr("Look for:"), this);
    mygrid->addWidget(l1, 2, 0, 1, 4);

    searchkey = new QLineEdit();
    mygrid->addWidget(searchkey, 2, 4, 1, 4);
    connect(searchkey, SIGNAL(returnPressed()), SLOT(accept()));
    searchkey->setFocus();

    emcheck = new QCheckBox(tr("Exact matches only"));
    mygrid->addWidget(emcheck, 3, 0, 1, 8);

    QHBoxLayout *buttonHBox = new QHBoxLayout();
    QSpacerItem *spacer = new QSpacerItem(1, 1, QSizePolicy::Expanding, QSizePolicy::Minimum);

    buttonHBox->addItem(spacer);

    QPushButton *ok, *dismiss;

    ok = new QPushButton(tr("Search"));
    connect(ok, SIGNAL(clicked()), SLOT(accept()));
    buttonHBox->addWidget(ok);

    dismiss = new QPushButton(tr("Cancel"));
    connect(dismiss, SIGNAL(clicked()), SLOT(reject()));
    buttonHBox->addWidget(dismiss);

    mygrid->addLayout(buttonHBox, 4, 0, 1, 8);
}
