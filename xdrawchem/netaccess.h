// XDrawChem
// Copyright (C) 2004-2005  Bryan Herger <bherger@users.sourceforge.net>
// Copyright (C) 2020  Yaman Qalieh <ybq987@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef NETACCESS_H
#define NETACCESS_H

#include <QDialog>

class QString;
class QStringList;
class QProgressDialog;
class QHttp;
class QBuffer;

class NetAccess : public QDialog {
    Q_OBJECT

  public:
    NetAccess();
    QStringList getChoices(QString, QString, QString, bool);
    QString getFile(QString, QString);
    QString getCanonicalSmiles(QString, QString);
    QString Rearrange(QString);
    bool getNameCAS(QString, QString);
    bool get3DMol(QString);
    bool runBuild3D(QString);
    bool runInChI(QString);
    bool status;
    QString htfile, spccompound, sname, scas, siupacname, s3dmol, fullinchi, shortinchi;
    QHttp *http;

  public slots:
    void slotData(const QByteArray &);
    void slotFinished(int, bool);
    void rf(bool);

  private:
    // QProgressDialog *progressDialog;
    bool httpRequestAborted;
    QBuffer *buffer;
    int choicesId;

  private slots:
    void slotUpdateDataReadProgress(int, int);
    void slotCancelDownload();

  signals:
    void choicesFinished(const QStringList &);
};

#endif
