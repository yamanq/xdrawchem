// XDrawChem
// Copyright (C) 2004-2005  Bryan Herger <bherger@users.sourceforge.net>
// Copyright (C) 2020  Yaman Qalieh <ybq987@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "tooldialog.h"
#include "defs.h"
#include "helpwindow.h"
#include "render2d.h"

// defined in main.cpp
extern QString RingDir;

ToolDialog::ToolDialog(QWidget *parent) : QDialog(parent) {
    helpfile = "spectra.html";
    this_mol = 0;
    product_mol = 0;
    /* This should be done in sub-classes
       QGridLayout *mygrid;
       mygrid = new QGridLayout(this,2,2,5);
       QPushButton *qhelp = new QPushButton( tr("Help"), this);
       mygrid->addWidget(qhelp,2,1);
       connect(qhelp, SIGNAL(clicked()), SLOT(SendHelp()));
       QPushButton *qclose = new QPushButton( tr("Close"), this);
       mygrid->addWidget(qclose,2,2);
       connect(qclose, SIGNAL(clicked()), SLOT(accept()));
     */
}

void ToolDialog::process() { qDebug() << "Nothing happens"; }

void ToolDialog::setMolecule(Molecule *m1) {
    this_mol = m1;
    process();
}

void ToolDialog::setMolecule(OBMol *ob1) {
    this_mol = new Molecule(0);
    this_mol->convertFromOBMol(ob1);
    process();
}

// note: must set product molecule first for correct behavior!
void ToolDialog::setProductMolecule(Molecule *m1) { product_mol = m1; }

void ToolDialog::setProductMolecule(OBMol *ob1) {
    product_mol = new Molecule(0);
    product_mol->convertFromOBMol(ob1);
}

void ToolDialog::SendHelp() {
    QString home;

#ifdef UNIX
    home = RingDir + "doc/" + helpfile;
#else
    home = RingDir + "doc\\" + helpfile;
#endif
    HelpWindow *help = new HelpWindow(home, ".", 0);

    help->setWindowTitle(QString(XDC_VERSION) + " - Help viewer");
    help->show();
}

// cmake#include "tooldialog.moc"
