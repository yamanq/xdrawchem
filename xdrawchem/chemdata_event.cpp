// XDrawChem
// Copyright (C) 2004-2005  Bryan Herger <bherger@users.sourceforge.net>
// Copyright (C) 2020  Yaman Qalieh <ybq987@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <QList>

#include "arrow.h"
#include "bond.h"
#include "bracket.h"
#include "chemdata.h"
#include "curvearrow.h"
#include "defs.h"
#include "drawable.h"
#include "gobject.h"
#include "molecule.h"
#include "symbol.h"
#include "text.h"

void ChemData::XDCEventHandler(XDC_Event *evt) {
    GraphicObject *go;

    switch (evt->type()) {
    case EVT_ADD_BOND:
        if (evt->Param2() < 0)
            addBond(evt->Start(), evt->End(), evt->Param1(), 1, evt->color(), evt->bool1());
        else
            addBond(evt->Start(), evt->End(), evt->Param1(), evt->Param2(), evt->color(),
                    evt->bool1());
        break;
    case EVT_ADD_BOND_UP:
        addBond(evt->Start(), evt->End(), 1, 5, evt->color());
        break;
    case EVT_ADD_BOND_DOWN:
        addBond(evt->Start(), evt->End(), 1, 7, evt->color());
        break;
    case EVT_ADD_BOND_WAVY:
        addBond(evt->Start(), evt->End(), 1, 6, evt->color());
        break;
    case EVT_ADD_BOND_DASH:
        addBond(evt->Start(), evt->End(), evt->Param1(), 99, evt->color());
        break;
    case EVT_ADD_ARROW:
        addArrow(evt->Start(), evt->End(), evt->color(), evt->Param1(), evt->Param2());
        break;
    case EVT_ADD_BRACKET:
        addBracket(evt->Start(), evt->End(), evt->color(), evt->Param1());
        break;
    case EVT_ADD_CURVEARROW:
        addCurveArrow(evt->Start(), evt->End(), evt->color(), evt->text());
        break;
    case EVT_ADD_GRAPHIC:
        go = new GraphicObject(r);
        go->setObjectType(TYPE_BEZIER); // for now!
        go->SetStyle(evt->Param1());
        go->SetColor(evt->color());
        go->setPointArray(evt->points());
        addGraphicObject(go);
        break;
    default:
        qDebug() << "Unknown event type???";
        break;
    }

    // delete the event!
    delete evt;
}
