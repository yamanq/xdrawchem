// XDrawChem
// Copyright (C) 2004-2005  Bryan Herger <bherger@users.sourceforge.net>
// Copyright (C) 2020  Yaman Qalieh <ybq987@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef CML_H
#define CML_H

#include "defs.h"
#include "dpoint.h"

// temporary data structures to hold data in CML formats
class CML_Atom {
  public:
    CML_Atom() {
        x = 987654321.0;
        y = 123456789.0;
        element = QString("C");
        atom = true;
    }

    QString id;
    QString element;
    int formalCharge;
    double x, y, z;
    bool atom;

    DPoint *toDPoint() {
        DPoint *d = new DPoint;
        d->x = x;
        d->y = y;
        d->id = id;
        d->element = element;
        return d;
    }
};

class CML_Bond {
  public:
    CML_Bond() {
        order = 1;
        type = TYPE_BOND;
    }
    QString id;
    QString a1;
    QString a2;
    int type; // for non-line types
    int order;
};

class CDXML_Node {
  public:
    QString pos;
    QString text;
};

#endif
